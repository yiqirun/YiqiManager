import Vue from "vue";
import VueRouter from "vue-router";

const adminView = () => import("@/components/admin/adminView.vue");
const login = () => import("@/components/common/login.vue");
const tag = () => import("@/components/common/tag.vue");

const blogMgmt = () => import(`@/components/admin/blogMgmt.vue`);
const endUserMgmt = () => import(`@/components/admin/endUserMgmt.vue`);
const information = () => import(`@/components/admin/information.vue`);
const adminMenuMgmt = () => import(`@/components/admin/adminMenuMgmt.vue`);
const roleMgmt = () => import(`@/components/admin/roleMgmt.vue`);
const binaryFileMgmt = () => import(`@/components/admin/binaryFileMgmt.vue`);
const systemParameter = () => import(`@/components/admin/systemParameter.vue`);
const report = () => import(`@/components/admin/report.vue`);

Vue.use(VueRouter);

const routes = [
    {
        path: `/`,
        redirect: `/ui/admin`
    },
    {
        path: `/ui/login`,
        component: login
    },
    {
        path: `/ui/admin`,
        component: adminView,
        children: [
            {
                path: ``,
                redirect: `issue`
            },
            {
                path: `issue`,
                component: blogMgmt
            },
            {
                path: `tag`,
                component: tag
            },
            {
                path: `issue/:issueid`,
                component: blogMgmt
            },
            {
                path: `enduser`,
                component: endUserMgmt
            },
            {
                path: `adminmenu`,
                component: adminMenuMgmt
            },
            {
                path: `info`,
                component: information
            },
            {
                path: `role`,
                component: roleMgmt
            },
            {
                path: `picture`,
                component: binaryFileMgmt
            },
            {
                path: `sysparam`,
                component: systemParameter
            },
            {
                path: `report`,
                component: report
            },
        ]
    }
];

export default new VueRouter({
	//与spring-boot整合打包, 不能为history模式
    //mode: `history`,
    base: process.env.BASE_URL,
    routes
});
