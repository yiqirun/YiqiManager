import '@babel/polyfill';

import Vue from "vue";
import router from "@/router";
import store from "@/store";
import i18n from "@/i18n";

import axios from "axios";
import VueAxios from "vue-axios";

import "element-ui/lib/theme-chalk/index.css";
import Element from "element-ui";

Vue.use(Element);
Vue.use(VueAxios, axios);

Vue.config.productionTip = false;
Vue.config.devtools = true;

const vm = new Vue({
    el: `#app`,
    router,
    store,
    i18n,
    components: {
        //navigator
    }
});

Vue.use({ vm });

