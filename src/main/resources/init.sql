delete from public.admin_menu;
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (1,  '权限管理',        null,                   null);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (11, '个人信息',        '/ui/admin/info',       1);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (12, '功能菜单',        '/ui/admin/adminmenu',  1);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (13, '团队管理',        '/ui/admin/role',       1);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (14, '成员管理',        '/ui/admin/enduser',    1);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (15, '项目管理',        '/ui/admin/project',    1);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (16, '系统参数',        '/ui/admin/project',    1);


INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (2,  '敏捷管理',      null,                   null);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (21, '迭代计划',      '/ui/admin/issue',       2);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (22, '故事管理',      '/ui/admin/issue',       2);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (23, 'Issue管理',     '/ui/admin/issue',       2);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (24, '燃尽图',        '/ui/admin/issue',       2);

--INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (3,  '向',       null,                   null);
--INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (22, '故障管理',        '/ui/admin/issue',       2);
--INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (23, '统计报表',        '/ui/admin/report',      2);

INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (9,  '实用工具',        null,                   null);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (91, '统计报表',        '/ui/admin/report',      9);
INSERT INTO public.admin_menu(id, title, url, father_id) VALUES (92, '贡献统计',        '/ui/admin/report',      9);


delete from public.role;
INSERT INTO public.role(id, role_name) VALUES (1, 'ADMIN');
INSERT INTO public.role(id, role_name) VALUES (2, 'TEST');
INSERT INTO public.role(id, role_name) VALUES (3, 'MANAGER');
INSERT INTO public.role(id, role_name) VALUES (4, 'CODER');

delete from public.role_menu_relation;
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (1, 11);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (1, 12);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (1, 13);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (1, 14);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (1, 22);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (1, 23);

INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (2, 11);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (2, 22);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (2, 23);

INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (3, 11);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (3, 14);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (3, 22);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (3, 23);

INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (4, 11);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (4, 22);
INSERT INTO public.role_menu_relation(role_id, menu_id) VALUES (4, 23);

delete from public.end_user;
INSERT INTO public.end_user(id, password, username, email) VALUES ( 1, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'admin',       '33865122@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 2, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'wuxianfei',   'rocky-wu@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 3, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'fanzhenzhi',  'fanzhenzhi@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 4, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'fangshijian', 'fangshijian@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 5, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'fangli',      'fanli16@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 6, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'jiangshishuang', 'jiangshishuang1@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 7, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'liqinghua',   'liqinghua3@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 8, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'maofangxiu',  'maofangxiu@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES ( 9, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'qiankaiyuan', 'qiankaiyuan1@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES (10, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'wangjianwei', 'javid.wangjianwei@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES (11, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'wangyanzhao', 'wangyanzhao7@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES (12, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'yangshuangsheng', 'yangshuangsheng@qq.com');
INSERT INTO public.end_user(id, password, username, email) VALUES (13, '$2a$10$oowG1v7D14PWZXPlfTd3hu0q3XnuSq0xT4QYwnyFJbMebUW6Wz.9C', 'zhumi',       'zhumi@qq.com');

delete from public.enduser_role_relation;
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (1, 1);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (2, 1);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (3, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (4, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (5, 2);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (5, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (6, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (7, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (8, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (9, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (10, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (11, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (12, 4);
INSERT INTO public.enduser_role_relation(enduser_id, role_id) VALUES (13, 4);
