package run.yiqi.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateTimeUtil {
    public static final String DATETIMEFORMAT001 = "yyyyMMddHHmmssSSS";
    public static final String DATETIMEFORMAT002 = "yyyy-MM-dd HH:mm:ss";
    public static final String DATETIMEFORMAT003 = "yyyy-MM-dd";

    public static final String getDateTimeStr_yyyyMMddHHmmssSSS() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATETIMEFORMAT001));
    }
}
