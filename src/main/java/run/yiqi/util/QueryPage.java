package run.yiqi.util;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;


/**
 * 分页器
 * @author Administrator
 *
 */
@Slf4j
@Setter
@Getter
@ToString
public class QueryPage {
    public static final int DEFAULT_CURRENT_PAGE = 1;
    public static final int DEFAULT_PAGE_SIZE = 10;
    public static final int DEFAULT_TOTAL = 0;

    private int pageSize = DEFAULT_PAGE_SIZE;
    private long total = DEFAULT_TOTAL;
    private int currentPage = DEFAULT_CURRENT_PAGE;
    
    public int getPages() {
        int pages = (int) Math.ceil((Double.valueOf(this.total) / Double.valueOf(this.pageSize)));
        if(pages == 0) pages++;
        log.info(">>>>pages = {}", pages);
        
        return pages;
    }
    
}
