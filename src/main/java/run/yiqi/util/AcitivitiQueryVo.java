package run.yiqi.util;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class AcitivitiQueryVo {

    private String processDefinitionKey = null;
    private String prcessInstanceId = null;
    private String assignee = null;
    private Map<String, Object> variables = new HashMap<String, Object>();

}
