package run.yiqi.blog.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class IssueComment implements Serializable {
    private static final long serialVersionUID = 6961037288345418312L;

    //ID,自动生成唯一字符串
    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String id;

    //处理描述
    @Lob
    @Type(type="org.hibernate.type.TextType")
    @Basic(fetch = FetchType.EAGER)
    @Column(nullable = false)
    private String content;

    //对应的问题
    //主动方
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "issue_id", nullable = true)
    private Issue issue;
    
    //创建时间
    @CreatedDate
    @Column(nullable = true)
    private LocalDateTime createDate;

    //更新时间
    @LastModifiedDate
    @Column(nullable = true)
    private LocalDateTime updateDate;

    //创建人
    @CreatedBy
    private String createBy;

    //更新人
    @LastModifiedBy
    private String updateBy;

}
