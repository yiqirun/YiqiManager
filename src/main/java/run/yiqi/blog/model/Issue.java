package run.yiqi.blog.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
@EntityListeners(AuditingEntityListener.class)
public class Issue implements Serializable {
    private static final long serialVersionUID = 6961037288345418312L;

    //ID,自动生成唯一字符串
    @Id
    @GenericGenerator(name = "sequence", strategy = "sequence")
    @GeneratedValue(generator = "sequence")
    private Integer id;

    //问题标题
    @Column(nullable = false)
    private String title;

    //问题描述
    @Lob
    @Type(type="org.hibernate.type.TextType")
    @Basic(fetch = FetchType.LAZY)
    @Column(nullable = false)
    private String content;
    
    //问题的状态: 1: Open, 2: Ready to test, 3: Close 4: Discard 5: Draft
    //状态的更新时间取updateDate, 如果状态为关闭,则updateDate为关闭时间
    private Integer status;
    
        
    //每个问题对应的评论列表
    //被动方
    @OneToMany(cascade = {CascadeType.REMOVE, CascadeType.PERSIST}, fetch = FetchType.LAZY, mappedBy = "issue")
    private List<IssueComment> issueComments = new ArrayList<>();
    
    //主动方
    //问题对应的标签
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinTable(name = "issue_tag_relation", joinColumns = @JoinColumn(name = "issue_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "optionid"))
    private Set<TagOption> tags = new HashSet<TagOption>();
    
    //问题发现人,或者问题提出人
    private String issueBy;
    
    //问题发现的时间, 或者问题提出的时间
    @Column(nullable = true)
    private LocalDateTime issueDate;
    
    //问题的处理人
    private String handleBy;
    
    //问题的期望解决日期
    @Column(nullable = true)
    private LocalDateTime handleDate;
    
    //问题的责任人, 也就是由谁导入的问题
    //private String importBy;

    //创建时间(注意: 问题发现时间 不等于 问题创建时间)
    @CreatedDate
    @Column(nullable = true)
    private LocalDateTime createDate;

    //更新时间
    @LastModifiedDate
    @Column(nullable = true)
    private LocalDateTime updateDate;

    //创建人(注意: 问题发现人 不等于 问题创建人)
    @CreatedBy
    private String createBy;

    //更新人
    @LastModifiedBy
    private String updateBy;

}
