package run.yiqi.blog.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
public class TagOption implements Serializable {
    private static final long serialVersionUID = -4461374781354762002L;

    @Id
    @GenericGenerator(name = "uuid", strategy = "uuid")
    @GeneratedValue(generator = "uuid")
    private String optionid;

    @Column(nullable = true)
    private String optionname;
    
    /**
     * 主动方
     */
    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "tagtype_id", nullable = true)
    private TagType tagtype;
    
    /**
     * 被动方
     */
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "tags")
    private List<Issue> issues = new ArrayList<Issue>();
    
    @Column(nullable = true)
    private int sortOrder = 0;
}
