package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.Issue;


public interface IssueDao extends JpaSpecificationExecutor<Issue>, JpaRepository<Issue, Integer> {
}
