package run.yiqi.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import run.yiqi.blog.model.IssueComment;



public interface IssueCommentDao extends JpaSpecificationExecutor<IssueComment>, JpaRepository<IssueComment, String> {
}
