package run.yiqi.blog;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.stereotype.Component;

import run.yiqi.util.Util;


/**
 * 拦截所有的Reponse,加上当前用户作为请求头
 * @author Administrator
 *
 */
@Component
@ServletComponentScan
@WebFilter(urlPatterns="/*", filterName="responseHeaderFilter")
public class ResponseFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse hsr = (HttpServletResponse) response;
        
        try {
            hsr.setHeader("currentuser", Util.getCurrentUsername());
        } catch(Exception e) {
            /*donothing*/
        }
        chain.doFilter(request, response);

    }

}
