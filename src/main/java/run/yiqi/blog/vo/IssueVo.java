package run.yiqi.blog.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class IssueVo implements Serializable {
    private static final long serialVersionUID = -520335175185023815L;

    private Integer id;
    private String title;
    private String content;
    private Integer status;
    
    private List<IssueCommentVo> issueComments = new ArrayList<IssueCommentVo>();
    private List<TagOptionVo> tags = new ArrayList<TagOptionVo>();

    private String issueBy;
    private String issueDate;

    private String handleBy;
    private String handleDate;
    
    //private String importBy;

    private String createBy;
    private String createDate;

    private String updateBy;
    private String updateDate;
    
    public String getStatusLabel() {
        String label = null;
        switch(status) {
            case 1: 
                label = "Open";
                break;
            case 2:
                label = "Ready to test";
                break;
            case 3:
                label = "Close";
                break;
            case 4:
                label = "Discard";
                break;
            case 5:
                label = "Draft";
                break;
            default: 
                label = "";
                break;
        }
        return label;
    }
}
