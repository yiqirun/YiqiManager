package run.yiqi.blog.vo;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class IssueCommentVo implements Serializable {
    private static final long serialVersionUID = -520335175185023815L;

    private String id;
    private String content;
    private Integer issueid;
    
    private String createDate;
    private String updateDate;
    private String createBy;
    private String updateBy;
    
    private String handleResult;
}
