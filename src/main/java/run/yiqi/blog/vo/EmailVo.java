package run.yiqi.blog.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import run.yiqi.blog.service.EmailService;

@Setter
@Getter
public class EmailVo implements Serializable {
    private static final long serialVersionUID = -5550216144834136641L;

    private String subject;
    private String content;
    private List<String> tos = new ArrayList<String>();
    private List<String> ccs = new ArrayList<String>();
    private int contentType = EmailService.TYPE_HTML;
}
