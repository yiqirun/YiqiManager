package run.yiqi.blog;

import java.util.Optional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.extern.slf4j.Slf4j;
import run.yiqi.util.Util;

@Slf4j
@EnableAsync
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
@EnableJpaRepositories
@EnableJpaAuditing(auditorAwareRef = "endUserAuditorBean")
public class BlogApplication {

    public static void main(String[] args) {
        log.debug("starting.......");
        SpringApplication.run(BlogApplication.class, args);
    }

    
    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper().disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    }
    
    
}


/**
 * 审计功能
 * @author 老伍
 *
 */
@Slf4j
@Component
class EndUserAuditorBean implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
        log.debug("auditing...");
        String username = null;
        try {
            username = Util.getCurrentUsername();
        } catch(Exception e) {
            e.printStackTrace();
            return null;
        }
        
        return Optional.of(username);
    }

}

