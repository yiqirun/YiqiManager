package run.yiqi.blog;

import org.activiti.api.task.runtime.events.TaskAssignedEvent;
import org.activiti.api.task.runtime.events.TaskCompletedEvent;
import org.activiti.api.task.runtime.events.listener.TaskRuntimeEventListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class WorkFlowTaskListener {
  @Bean
  public TaskRuntimeEventListener<TaskAssignedEvent> taskAssignedListener() {
      return taskAssigned -> log.info(">>>> Task Assigned "+
                  taskAssigned.getEntity().getName() + " to " + taskAssigned.getEntity().getAssignee());
  }
  
  @Bean
  public TaskRuntimeEventListener<TaskCompletedEvent> taskCompletedListener() {
      return taskCompleted->log.info(">>>> Task Completed " +
                  taskCompleted.getEntity().getName() + " by " + taskCompleted.getEntity().getOwner());
  }
}
