package run.yiqi.blog.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder()
                            .title("YiqiManager Swagger-API文档")
                            .description("YiqiManager Swagger-API文档")
                            .termsOfServiceUrl("https://yiqi.run")
                            .version("1.0")
                            .contact(new Contact("laowu", "https://yiqi.run", "33865122@qq.com"))
                        .build())
                .select()
                .apis(RequestHandlerSelectors.basePackage("run.yiqi.blog.controller"))
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(this.securitySchemes())
                .securityContexts(this.xBasicSecurityContexts())
                ;
    }

    /**
     * @return
     */
    private List<BasicAuth> securitySchemes() {
        return Arrays.asList(new BasicAuth[] {
                new BasicAuth("UserNameNPassword")
        });
    }

    /**
     * basic auth使用
     * 
     * @return
     */
    private List<SecurityContext> xBasicSecurityContexts() {
        return Arrays.asList(SecurityContext.builder()
            .securityReferences(
                    Arrays.asList(new SecurityReference[] {
                            new SecurityReference("xBasic",  new AuthorizationScope[0])
                    })
            )
            .forPaths(PathSelectors.ant("*"))
            .build())
            ;
    }

    /**
     * apikey使用
     * 
     * @return
     */
    @SuppressWarnings("unused")
    private List<SecurityContext> securityContexts() {
        return Arrays.asList(SecurityContext.builder()
                .securityReferences(this.defaultAuth())
                //.forPaths(PathSelectors.regex("^(?!query).*$")) 
                //所有包含query的接口不需要使用securitySchemes
                .forPaths(PathSelectors.regex("^(?!api).*$"))
                .build());
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return Arrays.asList(new SecurityReference[] { new SecurityReference("Authorization", authorizationScopes) });
    }

}
