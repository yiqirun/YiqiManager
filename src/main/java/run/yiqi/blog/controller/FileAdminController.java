package run.yiqi.blog.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class FileAdminController {

    @SneakyThrows
    @ResponseBody
    @RequestMapping(value="/api/admin/fileadmin/admin/uploadImg")
    public Map<String, Object> uploadImg(
            @RequestParam(value="file", required=true) MultipartFile file, 
            HttpServletRequest request, 
            HttpServletResponse response) {
        
        log.info("uploadImg");
        
        Map<String, Object> result = new HashMap<String,Object>();
        return result;
    }

}
